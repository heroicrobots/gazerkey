# INSTALL.md

2019/3/31 Robin Rowe

## Install Free Software

* Install C++: MS VC++ Community, MacOS Xcode or Linux C++

2. Download and install Android Studio
3. Download and install Unity3d (free, classic), do not download Unity Hub (new, has issues)
4. Download and install UE4

## Download and Install Libraries

1. libunistd

	git clone https://github.com/robinrowe/libunistd.git

2. Fltk

	git clone https://github.com/fltk/fltk.git
	cd fltk
	mkdir build
	cd build
	cmake .. -A x64

### Windows FLTK

If you see linker errors for "unresolved external symbol __imp___snprintf" in git Windows 10 Visual Studio, add legacy_stdio_definitions to fltk/CMakeLists.txt:

    project(FLTK)
    cmake_minimum_required(VERSION 2.6.3)
    link_libraries(legacy_stdio_definitions)

### Linux Ubuntu FLTK

	apt-get install libx11-dev libgl1-mesa-dev 
libglu1-mesa-dev/

If you see...

	[ 15%] Building CXX object src/CMakeFiles/fltk.dir/Fl.cxx.o
	In file included from /home/rower/code/fltk/src/Fl.cxx:26:0:
	/home/rower/code/fltk/FL/platform.H:53:14: fatal error: X11/Xlib.h: No such file or directory
	#    include <X11/Xlib.h>

Means needs libxll-dev. If you see a Windows.h error, rerun cmake after installing libx11-dev.

	./bin/fltk-config --ldstaticflags
		/usr/local/lib/libfltk.a -lX11 -lXext -lpthread -lXfixes -lm -ldl

To do later...

3. OpenCV
4. Json11

## Mac Installation

1. Download and install Mac Xcode from the AppStore.
2. Download and install Cmake
3. Download and install Unity3d (not Hub)
4. Walk through Unity3d tutorials to learn Unity
5. Build Unity Hello World

## Tutorials

Walk through Android Studio and Unity tutorials.

[Coding Unity](https://unity3d.com/learn/tutorials/topics/scripting/coding-unity-absolute-beginner)

[Android OpenGL](https://developer.android.com/training/graphics/opengl)

[Android OpenGL Noobs](https://medium.com/@xzan/opengl-a-noobs-guide-for-android-developers-5eed724e07ad)

[Android Getting Started](http://www.learnopengles.com/android-lesson-one-getting-started/)

[FLTK OpenGL: Erco Cheat Sheet](http://seriss.com/people/erco/fltk/#OpenGlSimple)

## Programs

Create the following programs. Building four versions of Gazerkey, in Android Java, C++, Unity3d and UE4.

1. Android HelloWorld
2. Android HelloOpenGL: Displays Hello World on a billboard or quad using OpenGL 
3. C++ HelloWorld
4. C++ HelloOpenGL: Displays Hello World on a billboard or quad using OpenGL and FLTK
5. Unity3d HelloUnity3d: Displays Hello World on a billboard or quad using OpenGL
6. Gazermenu v1: Given a menu description in JSON, displays menu on a billboard, arrow keys and Enter key or Esc or operate
7. Gazermenu v2: Add OpenCV gaze support to act as arrow keys, use gaze dwell time (default 2 seconds) to select 
8. Gazerkey: Displays keyboard on billboard, OpenCV detects gaze, outputs as you gaze-type

--0--
