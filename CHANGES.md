# CHANGES.md

2019/3/15 Robin Rowe

## To Do

* hellogl: Display text 
	https://github.com/tlorach/OpenGLText.git
* hellogl: Display image on quad
	https://github.com/RaZeR-RBI/soil.git
* hellogl: Display image on billboard
* hellogl: Display 3D OBJ file
* glMenu: Construct menu using quads
* glMenu: Highlight menu items using arrow-key or mouse
* JsonMenuTest: Given a menu description in JSON, parse and output to console
* GazerMenuTest: Display menu on a billboard. Uses arrow keys and Enter key or Esc to operate, for testing without any gaze features.
* GazerKeyTest: Displays keyboard on billboard, OpenCV detects gaze, outputs as you gaze-type
* Gazerkey Hands-free UI: Add OpenCV gaze support to act as arrow keys, use gaze dwell time (default 2 seconds) to select 
* Gazerkey UI Features
	* Pointer (like a mouse)
	* Menu
	* Image deck menu (like in iTunes), horizontal or vertical
	* Snapshot image
	* Record video
	* Record audio-only
	* Whiteboard
	* Markers (Crosshairs on billboard)
	* VTC
	* Checklist
	* Checklist builder
	* Bounding box
	* SLAM
	* 3D display
	* 2D display
	* Labels
	* Slide show (like PPT)
	* Status bar
	* Dials (pie charts/donuts)
	* HUD
	* Virtual keyboards
	* 3D reconstruction with OpenDroneMap or AliceVision
* Remote UI
	* Create MQTT comms channels

	/ui
	/ui/menu
	/ui/mouse
	/ui/keyboard
	/ui/settings

	* Enable glMenu to broadcast UI events to subscribers
	* Enable subscribers to send a command asking to take control of UI, like MS Remote Desktop can. Then remotely move the mouse pointer, for example. 
	* In the settings channel the remote users may get or set glMenu UI elements, create or modify menus.
	* Create command line utility like Mosquitto mosquitto_sub 
	* Create an FLTK remote desktop GUI sort of like MS RemoteDesktop

## Completed 2019

* 4/21 Robin: Research OpenGL text, choose OpenGLText
* 4/6 Robin: Added hellogl program
* 4/4 Robin: Fix FLTK hello and sweep to build on Ubuntu
* 3/31 Robin: Added INSTALL.md, GettingStarted.md
* 3/29 Robin: Added FLTK hello and sweep code
* 3/15 Lu: Installed all tools (Mac)
* 3/14 Lu: Joined gazerkey project, git config

--0--
