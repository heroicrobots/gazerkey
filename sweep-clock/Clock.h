// clock: Clock.h
// FLTK simple line drawing animation

#ifndef Clock_h
#define Clock_h

#include <stdio.h>
#include <time.h>
#include <string>
#include <FL/Fl.H>
#include <FL/Fl_Double_Window.H>
#include <FL/Fl_Box.H>
#include <FL/fl_draw.H>
#include "Sweeper.h"

class Clock
:	public Fl_Box 
{	const Fl_Color backgroundColor = FL_BLUE;
	const Fl_Color circleColor = FL_RED;
	const Fl_Color textColor = FL_YELLOW;
	const Fl_Color sweepColor = FL_GREEN;
	time_t start;
	time_t tick;
	const double delay = 0.25;
	std::string status;
	Sweeper sweeper;
	static void TimerCallback(void* userdata)
	{	Clock* p = (Clock*)userdata;
		if(!p)
		{	return;
		}
		p->redraw();
		Fl::repeat_timeout(p->delay,TimerCallback,userdata);
	}
	void draw() 
	{	TickTock();
		DrawBackground();
		DrawSweeper();
		DrawCircle();
		DrawStatus();
	}
	void TickTock()
	{	tick = time(0) - start;
	}
	void DrawBackground()
	{	Fl_Box::draw();
	}
	void DrawSweeper()
	{	fl_color(sweepColor);
		sweeper.v.x = x() + w()/2;
		sweeper.v.y = y() + h()/2;
		sweeper.Move(long(tick),h()/2);
		sweeper.Draw();
	}
	void DrawCircle()
	{	fl_color(circleColor);
		const int cx = x() + w()/2;
		const int cy = y() + h()/2;
		fl_pie(cx-10,cy-10,20,20,0.0,360.0);
	}
	void DrawStatus()
	{	status = std::to_string(long(tick/60));
		status += ":";
		status += std::to_string(long(tick%60));
		fl_color(textColor);
		fl_font(FL_HELVETICA,16);
		fl_draw(status.c_str(),x()+4,y()+h()-4);
	}
public:
	Clock(int x,int y,int dx,int dy,const char* caption=0)
	:	Fl_Box(x,y,dx,dy,caption)
	{	box(FL_FLAT_BOX);
		color(backgroundColor);
		start = time(0);
		Fl::add_timeout(delay,TimerCallback,(void*)this);
	}
};

#endif
