// Vector2d.h

#ifndef Vector2d_h
#define Vector2d_h

#include <math.h>

//typedef int T;
template<typename T>
class Vector2d
{public:
	T x;
	T y;
	T dx;
	T dy;
	Vector2d()
	:	x(0)
	,	y(0)
	,	dx(0)
	,	dy(0)
	{}
};

#endif
