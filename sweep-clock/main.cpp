// clock: main.cpp

#include "Clock.h"

int main()
{	Fl_Double_Window window(220, 220, "Clock");
	const int border = 10;
	Clock clock(border,border,window.w()-2*border,window.h()-2*border);
	window.resizable(window);
	window.show();
	return Fl::run();
}
