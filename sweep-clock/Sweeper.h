// Sweeper.h

#ifndef Sweeper_h
#define Sweeper_h

#include <math.h>
#include <FL/Fl_Box.H>
#include "Vector2d.h"

class Sweeper
{public:
	Vector2d<int> v;
	void Move(long tick,double radius)
	{	const double pi = 3.14159f;
		const double theta = pi - (((tick % 60) / 60.f) * 2.f*pi);
		v.dx = int(sin(theta)*radius);
		v.dy = int(cos(theta)*radius);
	}
	void Draw()
	{	fl_line(v.x,v.y,v.x+v.dx,v.y+v.dy);
	}
};

#endif
