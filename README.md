# gazerkey

Gazerkey is an open source project on gitlab. We're building a hands-free AR UI controlled by gaze. A gaze-controlled, on-screen, virtual keyboard uses eye-tracking to press keys by looking at them. 

For an AR headset, such as HoloLens 2:

https://youtu.be/k5SMABo4jwM

