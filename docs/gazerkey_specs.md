# gazerkey_specs.md

2019/3/28 Robin Rowe

Gazerkey is a gaze-based UI for Augmented Reality (AR).

## GazerMenuTest

Given a menu description in JSON, displays menu on a billboard. Uses arrow keys and Enter key or Esc to operate, for testing without any gaze features.

## GazerKeyTest

Displays keyboard on billboard, OpenCV detects gaze, outputs as you gaze-type

## Gazerkey Hands-free UI

Add OpenCV gaze support to act as arrow keys, use gaze dwell time (default 2 seconds) to select 

1. Given a menu description in JSON, displays menu on a billboard, arrow keys and Enter key or Esc or operate.

2. Dwell time (default 2 seconds) to select menu item. When mouse pointer hovers over a menu choice, a small sweep clock appears as a visual countdown to the menu choice being activated.

## Gazerkey UI Elements

* Pointer (like a mouse)
* Menu
* Image deck menu (like in iTunes), horizontal or vertical
* Snapshot image
* Record video
* Record audio-only
* Whiteboard
* Markers (Crosshairs on billboard)
* VTC
* Checklist
* Checklist builder
* Bounding box
* SLAM
* 3D display
* 2D display
* Labels
* Slide show (like PPT)
* Status bar
* Dials (pie charts/donuts)
* HUD
* Virtual keyboards
* 3D reconstruction with OpenDroneMap or AliceVision

https://youtu.be/R0PDCp0QF1o
https://www.blendernation.com/2015/11/16/instant-meshes-a-free-qaud-based-autoretopology-program/
https://github.com/wjakob/instant-meshes
https://all3dp.com/1/best-photogrammetry-software/


## Remote UI

Using MQTT as comms channel, enable glMenu to broadcast UI events to subscribers. Also enable subscribers to send a command asking to take control of UI, like MS Remote Desktop can. Then remotely move the mouse pointer, for example. 

	git clone https://github.com/eclipse/mosquitto.git

MQTT channels

	/ui
	/ui/menu
	/ui/mouse
	/ui/keyboard
	/ui/settings

In the settings channel the remote users may get or set glMenu UI elements, create or modify menus.

1. Create command line utility like Mosquitto mosquitto_sub 

2. Create an FLTK remote desktop GUI

--0--
