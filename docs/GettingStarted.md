# CHANGES.md

2019/3/15 Robin Rowe

## Required

Set your own schedule. Procedure is a start-of-day email saying what you intend to do today. Then an end-of-day email saying what you did and what you intend to do next time. Too busy for slack. No pull requests because no branches. Everyone is trusted to work directly in master.

## Daily Emails and Push

Whenever you start a day that you're doing something with the project, send an email to let us know you're online and list what you plan to do today. 

At the end of your day, send an email that says what you did, what you intend to do next, and what day you plan to be back. 

Push commits at end-of-day before you send your email, if not more often. Try to remember to make separate commits per each task. If your commit breaks build, note that in email (rare). Do not neglect to make your end-of-day commit push. 

## Join Project

1. Join project, using email invitation received from gitlab
2. Create and install gitlab ssh key for yourself

## git

Create your own gitlab ssh key following instructions on their website.

## Windows git bash

	https://git-scm.com/downloads

## Using git

	git clone git@gitlab.com:heroicrobots/gazerkey.git
	cd gazerkey
	git config user.name "Your Name"
	git config user.email your.name@somewhere.edu
	git status
	git add -A
	git commit -a -m "Relevant description here"
	git pull
	git push

## CMake, CMaker, CTest

Use CMake and CMaker: CMaker is a simple code generator that writes cmake frameworks. Do not create CMake files by hand. CMaker is in libunistd:

	git clone https://github.com/robinrowe/libunistd.git

## Install Free Software

See INSTALL.md

IMPORTANT: We use cmaker from libunistd to create our Cmake and C++ class frameworks. Do not code Cmake files from scratch.
	
## Build Test Programs

	mkdir build
	cd build
	cmake .. -A x64

Open sln file in VC++ or run make in Linux.

1. hello-fltk: Example of simple FLTK program

2. sweep-clock: Example of FLTK sweep clock

3. fltk-gl: Display "Hello World" on a billboard or quad using OpenGL in FLTK

[FLTK OpenGL: Erco Cheat Sheet](http://seriss.com/people/erco/fltk/#OpenGlSimple)

## Future Example Programs

Create the following programs. Building four versions of Gazerkey, in Android Java, C++, Unity3d and UE4.

1. Android HelloWorld
2. Android HelloOpenGL: Displays Hello World on a billboard or quad using OpenGL 
3. C++ HelloWorld
4. C++ HelloOpenGL: Displays Hello World on a billboard or quad using OpenGL and FLTK
5. Unity3d HelloUnity3d: Displays Hello World on a billboard or quad using OpenGL

## Tutorials

Depending upon which platform you are developing upon, walk through Android Studio, Unity and UE4 tutorials.

[Coding Unity](https://unity3d.com/learn/tutorials/topics/scripting/coding-unity-absolute-beginner)

[Android OpenGL](https://developer.android.com/training/graphics/opengl)

[Android OpenGL Noobs](https://medium.com/@xzan/opengl-a-noobs-guide-for-android-developers-5eed724e07ad)

[Android Getting Started](http://www.learnopengles.com/android-lesson-one-getting-started/)

[FLTK OpenGL: Erco Cheat Sheet](http://seriss.com/people/erco/fltk/#OpenGlSimple)

--0--
