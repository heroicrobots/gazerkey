// GlWindow.h 
// Created by Robin Rowe 2019-04-06
// License MIT Open Source

#ifndef GlWindow_h
#define GlWindow_h

#include <iostream>
#include <FL/Fl.H>
#include <FL/Fl_Gl_Window.H>
#include <FL/gl.h>

class GlWindow
:	public Fl_Gl_Window 
{	GlWindow(const GlWindow&) = delete;
	void operator=(const GlWindow&) = delete;
	void Init()
	{	glLoadIdentity();
		glViewport(0,0,w(),h());
		glOrtho(-w(),w(),-h(),h(),-1,1);
	}
	void DrawX()
	{	glColor3f(1.0, 1.0, 1.0);
		glBegin(GL_LINE_STRIP); 
			glVertex2f(w(), h()); 
			glVertex2f(-w(),-h()); 
		glEnd();
		glBegin(GL_LINE_STRIP); 
			glVertex2f(w(),-h()); 
			glVertex2f(-w(), h()); 
		glEnd();
	}
    void draw() 
	{	if (!valid()) 
		{	Init();
		}
		glClear(GL_COLOR_BUFFER_BIT);
		DrawX();
	}
public:
	std::ostream& Print(std::ostream& os) const;
	~GlWindow()
	{}
    GlWindow(int X,int Y,int W,int H,const char*L=0) 
	:	Fl_Gl_Window(X,Y,W,H,L) 
	{}
	bool operator!() const
	{	return true;
	}
};

inline
std::ostream& operator<<(std::ostream& os,const GlWindow& glWindow)
{	return glWindow.Print(os);
}

#endif
