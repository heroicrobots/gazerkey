// test_GlWindow.cpp 
// Created by Robin Rowe 2019-04-06
// License MIT Open Source

#include <iostream>
#include "../GlWindow.h"
using namespace std;

int main(int argc,char* argv[])
{	cout << "Testing GlWindow" << endl;
	GlWindow glWindow(0,0,400,300);
	if(!glWindow)
	{	cout << "GlWindow failed, operator! == true" << endl;
		return 1;
	}
	cout << glWindow << endl << "Done!" << endl;
	return 0;
}
