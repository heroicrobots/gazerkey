// hellogl.cpp 
// Created by Robin Rowe 2019-04-06
// License MIT Open Source

#include <iostream>
#include "GlWindow.h"
using namespace std;

void Usage()
{	cout << "Usage: hellogl " << endl;
}

enum
{	ok,
	invalid_args

};

int main(int argc,char* argv[])
{	cout << "hellogl starting..." << endl;
	if(argc < 1)
	{	Usage();
		return invalid_args;
	}
	Fl_Window win(500, 300, "OpenGL X");
	GlWindow glWindow(10, 10, win.w()-20, win.h()-20);
	win.end();
	win.resizable(glWindow);
	win.show();
	const int ret = Fl::run();
	cout << "hellogl done!" << endl;
	return ok;
}

    