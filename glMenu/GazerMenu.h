// GazerMenu.h 
// Created by Robin Rowe 2019-03-28
// License MIT Open Source

#ifndef GazerMenu_h
#define GazerMenu_h

#include <iostream>

class GazerMenu
{	GazerMenu(const GazerMenu&) = delete;
	void operator=(const GazerMenu&) = delete;

public:
	std::ostream& Print(std::ostream& os) const;
	~GazerMenu()
	{}
	GazerMenu()
	{}
	bool operator!() const
	{	return true;
	}
};

inline
std::ostream& operator<<(std::ostream& os,const GazerMenu& gazerMenu)
{	return gazerMenu.Print(os);
}

#endif
