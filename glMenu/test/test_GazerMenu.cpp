// test_GazerMenu.cpp 
// Created by Robin Rowe 2019-03-28
// License MIT Open Source

#include <iostream>
#include "../GazerMenu.h"
using namespace std;

int main(int argc,char* argv[])
{	cout << "Testing GazerMenu" << endl;
	GazerMenu gazerMenu;
	if(!gazerMenu)
	{	cout << "GazerMenu failed, operator! == true" << endl;
		return 1;
	}
	cout << gazerMenu << endl << "Done!" << endl;
	return 0;
}
