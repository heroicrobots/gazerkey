// gazerkey.cpp 
// Created by Robin Rowe 2019-03-28
// License MIT Open Source

#include <iostream>
using namespace std;

void Usage()
{	cout << "Usage: gazerkey " << endl;
}

enum
{	ok,
	invalid_args

};

int main(int argc,char* argv[])
{	cout << "gazerkey starting..." << endl;
	if(argc < 1)
	{	Usage();
		return invalid_args;
	}

	cout << "gazerkey done!" << endl;
	return ok;
}
